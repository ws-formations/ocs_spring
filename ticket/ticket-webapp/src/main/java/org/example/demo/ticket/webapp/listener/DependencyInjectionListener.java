package org.example.demo.ticket.webapp.listener;

import org.example.demo.ticket.business.impl.ManagerFactory;
import org.example.demo.ticket.business.impl.manager.ProjetManagerImpl;
import org.example.demo.ticket.business.impl.manager.TicketManagerImpl;
import org.example.demo.ticket.webapp.rest.resource.AbstractResource;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * our injecter l'instance de ManagerFactory, afin que toutes les instances de Resource y aient accès,
 * il ne reste plus qu'à faire appel au setter staticAbstractResource.setManagerFactory(ManagerFactory)
 *
 * DependencyInjectionListener implémentant l'interface ServletContextListener :
 *
 * Et voilà, je viens de mettre en place une injection de dépendances :
 *
 * 1- Au déploiement de l'application, la classe DependencyInjectionListener est instanciée par le serveur d'application.
 *
 * 2- Le serveur appelle ensuite la méthode contextInitialized(ServletContextEvent) sur l'instance qu'il vient de créer.
 *
 * 3- Cette méthode crée l'instance de ManagerFactory et l'injecte dans l'attribut static dédié (managerFactory)
 *    de la classe AbstractResource.
 *
 * les classes Resource disposent enfin d'une instance de ManagerFactory (via la méthode staticgetManagerFactory())
 * sans avoir à se soucier de comment créer/récuperer cette instance !
 *
 * Pour lcette etape plus besoin de cette classe
 * public class DependencyInjectionListener implements ServletContextListener {
 *   @Override
 *   public void contextInitialized(ServletContextEvent pServletContextEvent) ;
 *   @Override
 *   public void contextDestroyed(ServletContextEvent servletContextEvent)
 * }
 */
public class DependencyInjectionListener {

    // @Override
    public void contextInitialized(ServletContextEvent pServletContextEvent) {
        // Création de l'instance de ManagerFactory
       // ManagerFactory vManagerFactory = new ManagerFactory();

        // On ajoute l'injection des Managers dans la ManagerFactory
       // vManagerFactory.setProjetManager(new ProjetManagerImpl());
        //vManagerFactory.setTicketManager(new TicketManagerImpl());

        // Injection de l'instance de ManagerFactory dans la classe AbstractResource
       // AbstractResource.setManagerFactory(vManagerFactory);
    }

   // @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
