package org.example.demo.ticket.business.impl;

import org.example.demo.ticket.business.contract.manager.ProjetManager;
import org.example.demo.ticket.business.contract.manager.TicketManager;

/**
 * l'inversion de contrôle (IoC).
 *
 * Je vais créer une classe ManagerFactory à laquelle on pourra demander les instances
 * des managers (ProjetManager, TicketManager...)
 */
public interface ManagerFactory {

    // On renvoie désormais simplement l'attribut projetManager
    ProjetManager getProjetManager();

    // On renvoie désormais simplement l'attribut ticketManager
    TicketManager getTicketManager();
}
