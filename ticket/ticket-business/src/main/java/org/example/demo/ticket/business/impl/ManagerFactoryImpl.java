package org.example.demo.ticket.business.impl;

import org.example.demo.ticket.business.contract.manager.ProjetManager;
import org.example.demo.ticket.business.contract.manager.TicketManager;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * l'inversion de contrôle (IoC).
 *
 * Je vais créer une classe ManagerFactory à laquelle on pourra demander les instances
 * des managers (ProjetManager, TicketManager...)
 */
@Named("managerFactory")
public class ManagerFactoryImpl implements ManagerFactory {

    // Ajout d'un attribut projetManager
    @Inject
    private ProjetManager projetManager;

    // Ajout d'un attribut ticketManager
    @Inject
    private TicketManager ticketManager;

    public ManagerFactoryImpl() {
    }

    // On renvoie désormais simplement l'attribut projetManager
    @Override
    public ProjetManager getProjetManager() {
        return projetManager;
    }

    // Ajout d'un setter pour l'attribut projetManager
    public void setProjetManager(ProjetManager pProjetManager) {
        projetManager = pProjetManager;
    }

    // On renvoie désormais simplement l'attribut ticketManager
    @Override
    public TicketManager getTicketManager() {
        return ticketManager;
    }

    // Ajout d'un setter pour l'attribut ticketManager
    public void setTicketManager(TicketManager ticketManager) {
        ticketManager = ticketManager;
    }
}
